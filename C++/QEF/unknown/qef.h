//----------------------------------------------------------------------------
// Quadric Error Function
// author: Ronen Tzur
// source: ThreeD
// url: http://www.sandboxie.com/misc/isosurf/isosurfaces.html
//----------------------------------------------------------------------------

#ifndef _QEF_H
#define _QEF_H

//#include <windows.h>
#include <math.h>
#include <string.h>

class IVector
{
public:
    IVector() : _x(0), _y(0), _z(0){}
    IVector(float x, float y, float z) : _x(x), _y(y), _z(z) {}
    virtual ~IVector(){}

    virtual float x() const = 0;
    virtual float y() const = 0;
    virtual float z() const = 0;
protected:
    float _x, _y, _z;
};

// Dérivée de la classe IVector
class Vector: public IVector
{
public:
    /**
     * Constructor.
     * Constructs a vector in 3-space with components (0,0,0).
     */
    inline Vector() : _x(0), _y(0), _z(0) {}

    /**
     * Constructor.
     * Constructs a vector in 3-space using an (x,y,z) specification.
     */
    inline Vector(float x, float y, float z) : _x(x), _y(y), _z(z) {}

    float x() const { return _x; }
    float y() const { return _y; }
    float z() const { return _z; }

protected:
    float _x, _y, _z;
};

#ifdef __cplusplus
extern "C" {

#endif

IVector *Create( float x, float y, float z );

float X( IVector* pObject );
float Y( IVector* pObject );
float Z( IVector* pObject );

/**
 * QEF, a class implementing the quadric error function
 *      E[x] = P - Ni . Pi
 *
 * Given at least three points Pi, each with its respective
 * normal vector Ni, that describe at least two planes,
 * the QEF evalulates to the point x.
 */
void Evaluate(
        double mat[][3], double *vec, int rows,
        Vector *point);

    // compute svd

    void computeSVD(
        double mat[][3],                // matrix (rows x 3)
        double u[][3],                  // matrix (rows x 3)
        double v[3][3],                 // matrix (3x3)
        double d[3],                    // vector (1x3)
        int rows);

    // factorize

    void factorize(
        double mat[][3],                // matrix (rows x 3)
        double tau_u[3],                // vector (1x3)
        double tau_v[2],                // vectors, (1x2)
        int rows);

    double factorize_hh(double *ptrs[], int n);

    // unpack

    void unpack(
        double u[][3],                  // matrix (rows x 3)
        double v[3][3],                 // matrix (3x3)
        double tau_u[3],                // vector, (1x3)
        double tau_v[2],                // vector, (1x2)
        int rows);

    // diagonalize

    void diagonalize(
        double u[][3],                  // matrix (rows x 3)
        double v[3][3],                 // matrix (3x3)
        double tau_u[3],                // vector, (1x3)
        double tau_v[2],                // vector, (1x2)
        int rows);

    void chop(double *a, double *b, int n);

    void qrstep(
        double u[][3],                  // matrix (rows x cols)
        double v[][3],                  // matrix (3 x cols)
        double tau_u[],                 // vector (1 x cols)
        double tau_v[],                 // vector (1 x cols - 1)
        int rows, int cols);

    void qrstep_middle(
        double u[][3],                  // matrix (rows x cols)
        double tau_u[],                 // vector (1 x cols)
        double tau_v[],                 // vector (1 x cols - 1)
        int rows, int cols, int col);

    void qrstep_end(
        double v[][3],                  // matrix (3 x 3)
        double tau_u[],                 // vector (1 x 3)
        double tau_v[],                 // vector (1 x 2)
        int cols);

    double qrstep_eigenvalue(
        double tau_u[],                 // vector (1 x 3)
        double tau_v[],                 // vector (1 x 2)
        int cols);

    void qrstep_cols2(
        double u[][3],                  // matrix (rows x 2)
        double v[][3],                  // matrix (3 x 2)
        double tau_u[],                 // vector (1 x 2)
        double tau_v[],                 // vector (1 x 1)
        int rows);

    void computeGivens(
        double a, double b, double *c, double *s);

    void computeSchur(
        double a1, double a2, double a3,
        double *c, double *s);

    // singularize

    void singularize(
        double u[][3],                  // matrix (rows x 3)
        double v[3][3],                 // matrix (3x3)
        double d[3],                    // vector, (1x3)
        int rows);

    // solve svd
    void solveSVD(
        double u[][3],                  // matrix (rows x 3)
        double v[3][3],                 // matrix (3x3)
        double d[3],                    // vector (1x3)
        double b[],                     // vector (1 x rows)
        double x[3],                    // vector (1x3)
        int rows);

#ifdef __cplusplus
}
#endif

#endif // _QEF_H
