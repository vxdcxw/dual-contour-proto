//----------------------------------------------------------------------------
// VirtualMesh
// author: Ronen Tzur
// source: ThreeD
// url: http://www.sandboxie.com/misc/isosurf/isosurfaces.html
//----------------------------------------------------------------------------

int test;

#ifndef _ADDP_H
#define _ADDP_H

#include <windows.h>
#include <math.h>

#include <map>
#include <list>

#include<stdio.h>

/*  To use this exported function of dll, include this header
 *  in your project.
 */

#ifdef BUILD_DLL
    #define DLL_EXPORT __declspec(dllexport)
#else
    #define DLL_EXPORT __declspec(dllimport)
#endif // BUILD_DLL

class IVector
{
public:
    IVector() : _x(0), _y(0), _z(0){}
    IVector(float x, float y, float z) : _x(x), _y(y), _z(z) {}
    virtual ~IVector(){}

    virtual float x() const = 0;
    virtual float y() const = 0;
    virtual float z() const = 0;
protected:
    float _x, _y, _z;
};

// D�riv�e de la classe IVector
class Vector: public IVector
{
public:
    /**
     * Constructor.
     * Constructs a vector in 3-space with components (0,0,0).
     */
    inline Vector() : _x(0), _y(0), _z(0) {}

    /**
     * Constructor.
     * Constructs a vector in 3-space using an (x,y,z) specification.
     */
    inline Vector(float x, float y, float z) : _x(x), _y(y), _z(z) {}

    /**
     * Operator=.
     *
     * @return this vector, after assigning the @p other vector to it.
     */
    inline Vector &operator=(const Vector &other)
    {
        memcpy(this, &other, sizeof(Vector));
        //_x = other._x;
        //_y = other._y;
        //_z = other._z;
        return *this;
    }

    /**
     * Computes and returns the length of a vector, which is the square-root
     * of the sum of the squares of the x, y and z coordinates.
     *
     * @returns the length of a vector.
     */
    double length() const { return sqrt(_x * _x + _y * _y + _z * _z); }

    /**
     * @return a vector that is a normalization of this vector.
     */
    Vector normalized() const
    {
        double len = length();
        if (len == 1.0 || len == 0.0)
            return *this;
        else
            return (*this / len);
    }
    /**
     * @return the sum of the three components
     */
    operator float() const { return (float)(_x + _y + _z); }

    /**
     * @return the sum of the three components
     */
    operator double() const { return (double)(_x + _y + _z); }
    /**
     * X= operators.
     */
    Vector &operator+=(const Vector &v) { return operator=(*this + v); }
    Vector &operator/=(float f) { return operator=(*this / f); }
    /**
     * Comparison operators.
     */
    friend inline bool operator==(const Vector &a, const Vector &b);

    /**
     * Binary operators.
     */
    friend inline Vector operator+(const Vector &a, const Vector &b);
    friend inline Vector operator-(const Vector &a, const Vector &b);
    friend inline Vector operator*(const Vector &a, const Vector &b);
    friend inline Vector operator/(const Vector &v, float f);
    friend inline Vector operator*(const Vector &v, double f);
    friend inline Vector operator/(const Vector &v, double f);

    float x() const { return _x; }
    float y() const { return _y; }
    float z() const { return _z; }

protected:
    float _x, _y, _z;
};

// Friends
inline bool operator==(const Vector &a, const Vector &b)
{
    return (a._x == b._x) && (a._y == b._y) && (a._z == b._z);
}

inline Vector operator+(const Vector &a, const Vector &b)
{
    return Vector( (a._x + b._x), (a._y + b._y), (a._z + b._z) );
}

inline Vector operator-(const Vector &a, const Vector &b)
{
    return Vector( (a._x - b._x), (a._y - b._y), (a._z - b._z) );
}

inline Vector operator*(const Vector &a, const Vector &b)
{
    return Vector( (a._x * b._x), (a._y * b._y), (a._z * b._z) );
}

inline Vector operator/(const Vector &v, float f)
{
    return v * (1.0 / f);
}

inline Vector operator/(const Vector &v, double f)
{
    return Vector((float)(v._x / f), (float)(v._y / f), (float)(v._z / f));
}

inline Vector operator*(const Vector &v, double f)
{
    return Vector( (float)(v._x * f),
                   (float)(v._y * f),
                   (float)(v._z * f) );
}

/**
 * Plane, the mathematical entity defined by a point and a normal vector.
 */
class Plane
{
public:
    /**
     * Constructor.
     *
     * Constructs an XY plane.
     */
    Plane() : _a(0.0), _b(0.0), _c(1.0), _d(0.0) {}

    /**
     * Constructor.
     *
     * Constructs a plane using the components @p a, @p b, @p c and @p d.
     */
    Plane(double a, double b, double c, double d)
        : _a(a), _b(b), _c(c), _d(d) {}

    /**
     * Constructor.
     *
     * Constructs a plane using the three vertices @p v0, @p v1 and @p v2.
     */
    Plane(const Vector &v0, const Vector &v1, const Vector &v2)
    {
        // _abc = (v0 - v1) % (v1 - v2);
        double v0_v1_x = (v0 - v1).x();
        double v0_v1_y = (v0 - v1).y();
        double v0_v1_z = (v0 - v1).z();
        double v1_v2_x = (v1 - v2).x();
        double v1_v2_y = (v1 - v2).y();
        double v1_v2_z = (v1 - v2).z();
        _a = (v0_v1_y * v1_v2_z) - (v0_v1_z * v1_v2_y);
        _b = (v0_v1_z * v1_v2_x) - (v0_v1_x * v1_v2_z);
        _c = (v0_v1_x * v1_v2_y) - (v0_v1_y * v1_v2_x);
        // _d = -(v0 * _abc);
        _d = ( (v0.x() * _a) + (v0.y() * _b) + (v0.z() * _c) );
    }

    /**
     * Constructor.
     *
     * Constructs a plane to be a copy of the @p model plane.
     */
    Plane(const Plane &model) { operator=(model); }

    /**
     * @return a plane that is a normalization of this plane.
     */
    Plane normalized() const
    {
        double len = sqrt(_a * _a + _b * _b + _c * _c);
        if (len == 0.0)
            len = 1.0;
        return Plane(_a / len, _b / len, _c / len, _d / len);
    }

    /**
     * @return the A component for this plane.
     */
    double a() const { return _a; }

    /**
     * @return the B component for this plane.
     */
    double b() const { return _b; }

    /**
     * @return the C component for this plane.
     */
    double c() const { return _c; }

    /**
     * @return the D component for this plane.
     */
    double d() const { return _d; }

    /**
     * @return the normal for this plane.
     */
    const Vector normal() const
    {
        return Vector((float)_a, (float)_b, (float)_c);
    }

protected:
    /*
     * data
     */

    double _a;
    double _b;
    double _c;
    double _d;
};

/**
 * MeshFace
 */
class MeshFace
{
public:
    /**
     * Constructor.
     *
     * Constructs a mesh face as a triangle bound by the three vertices
     * @p v0, @p v1 and @p v2, and having color @p c.
     */
    MeshFace( const Vector *v0, const Vector *v1, const Vector *v2 )
    {
        _v[0] = v0;
        _v[1] = v1;
        _v[2] = v2;
        _planeNormal = Plane(*_v[2], *_v[1], *_v[0]).normal().normalized();
    }
    /**
     * Draws this mesh face.
     */
    void draw( Vector *points, Vector *normals )
    {
        int i;

        for (i = 0; i < 3; ++i) {
            const Vector &vertex = _v[i][0];
            const Vector &normal = _v[i][1];

            if ( normal == Vector() )
                normals[i] = Vector( planeNormal().x(), planeNormal().y(), -planeNormal().z() );
            else
                normals[i] = Vector( normal.x(), normal.y(), -normal.z() );

            points[i] = Vector( vertex.x(), vertex.y(), -vertex.z() );

            //glVertex3f(vertex.x(), vertex.y(), -vertex.z());
        }
    }

    /** Return pointer to vertex n. */
    const Vector *vertexPtr(int n) const { return _v[n]; }

    /** Return actual value of vertex n. */
    const Vector &vertex(int n) const { return *_v[n]; }

    /** Return plane. *//*
    const Plane plane() const
    {
        return Plane(vertex(2), vertex(1), vertex(0));
    }*/

    /** Return normalized plane normal. */
    const Vector planeNormal() const
    {
        return _planeNormal;
    }

public:

    /*
     * data
     */

    const Vector *_v[3];
    Vector _planeNormal;
};
typedef std::list<MeshFace *> FacesList;

struct MeshPlane {
    Plane p;
    FacesList faces;
};
typedef std::list<MeshPlane> PlanesList;

struct MeshPoint {
    Vector point;
    Vector normal;
    FacesList faces;
};
typedef std::multimap<unsigned long, MeshPoint> PointsMap;

// MeshFace
struct Triangle {
    int index;
    Vector *points;
    Vector *normals;
};


PointsMap _points;
PlanesList _planes;


#ifdef __cplusplus
extern "C" {

#endif
    DLL_EXPORT void Clear();

    // TRIANGLES
    DLL_EXPORT int GetTrianglesCount();
    DLL_EXPORT Triangle *Build();
    DLL_EXPORT Vector *GetTrianglePoints( Triangle *tri, int i, int p );
    DLL_EXPORT Vector *GetTriangleNormals( Triangle *tri, int i, int n );

    // VECTORS
    DLL_EXPORT Vector *CreateVectorPtr( float x, float y, float z );

    DLL_EXPORT MeshFace *CreateFacePtr( MeshPoint *p0, MeshPoint *p1, MeshPoint *p2 );

    DLL_EXPORT float X( Vector* pObject );
    DLL_EXPORT float Y( Vector* pObject );
    DLL_EXPORT float Z( Vector* pObject );

    /**
     * Defines a new point in this mesh.  This creates a new
     * point element unless such a point already exists in
     * the mesh.
     *
     * @return the index for the point.
     */
    DLL_EXPORT MeshPoint *AddPoint( const Vector &v  );
    DLL_EXPORT void SetMeshPointNormal( MeshPoint *mp, float x, float y, float z );

    /** Adds a plane to the mesh. */
    DLL_EXPORT MeshPlane *AddPlaneToVirtualMesh( MeshFace* face );

    DLL_EXPORT void AddFace(MeshFace *face, MeshPlane *mplane = 0);

    MeshPlane *addPlane( Plane plane1 );
#ifdef __cplusplus
}
#endif

#endif // _ADDP_H
