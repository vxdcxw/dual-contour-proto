//----------------------------------------------------------------------------
// VirtualMesh
// author: Ronen Tzur
// source: ThreeD
// url: http://www.sandboxie.com/misc/isosurf/isosurfaces.html
//----------------------------------------------------------------------------

#include "VirtualMesh.h"

//----------------------------------------------------------------------------

#define TOLERANCE 1e-3

//----------------------------------------------------------------------------

// TRIANGLES

int trianglesCount;


void Clear()
{
    // delete faces throughout all planes
    PlanesList::iterator itPlanes = _planes.begin();
    while (itPlanes != _planes.end()) {
        FacesList &faces = (*itPlanes).faces;
        while (! faces.empty()) {
            MeshFace *face = faces.front();
            delete face;
            faces.pop_front();
        }
        itPlanes = _planes.erase(itPlanes);
    }

    // delete points
    _points.clear();
}

int GetTrianglesCount()
{
    return trianglesCount;
}

void computeVertexNormals()
{
    PointsMap::iterator itPoints = _points.begin();

    while (itPoints != _points.end()) {
        MeshPoint &mpoint = (*itPoints).second;
        ++itPoints;

        Vector sum;
        int count = 0;

        FacesList &faces = mpoint.faces;

        if (! faces.empty()) {

            FacesList::iterator itFaces = faces.begin();
            MeshFace *face1 = (*itFaces);
            sum += face1->planeNormal();

            ++itFaces;

            while (itFaces != faces.end()) {

                MeshFace *face2 = (*itFaces);
                float dotprod = face1->planeNormal() * face2->planeNormal();

                if (dotprod > -0.92 && dotprod < 0.92) {
                    count = 0;
                    break;
                } else {
                    sum += face2->planeNormal();
                    ++count;
                    ++itFaces;
                }
            }
        }

        //count = 0;

        if (count > 0) {
            sum /= (float) count;
            mpoint.normal = sum.normalized();
        } else
            mpoint.normal = Vector();
    }
}

Triangle *Build()
{
    trianglesCount = 0;

    computeVertexNormals();

    Triangle *triangles = (Triangle*) malloc( sizeof( Triangle ) * 1 );

    PlanesList::const_iterator itPlanes = _planes.begin();

    while (itPlanes != _planes.end()) {

        const FacesList &faces = (*itPlanes).faces;
        FacesList::const_iterator itFaces = faces.begin();

        while (itFaces != faces.end()) {

            triangles = (Triangle*) realloc( triangles , sizeof( Triangle ) * ( trianglesCount + 1 ) );
            triangles[ trianglesCount ].points = (Vector*) malloc( sizeof( Vector ) * 3 );
            triangles[ trianglesCount ].normals = (Vector*) malloc( sizeof( Vector ) * 3 );
            triangles[ trianglesCount ].index = trianglesCount;

            //(*itFaces)->draw();
            (*itFaces)->draw( triangles[ trianglesCount ].points, triangles[ trianglesCount ].normals );

            ++trianglesCount;
            ++itFaces;
        }
        ++itPlanes;
    }

    return triangles;
}

Vector *GetTrianglePoints( Triangle *tri, int idx, int point )
{
    return &tri[idx].points[ point ];
}

 Vector *GetTriangleNormals( Triangle *tri, int idx, int normal )
{
    return &tri[idx].normals[ normal ];
}

// VECTOR

 Vector *CreateVectorPtr( float x, float y, float z )
{
    return new Vector( x, y, z );
}


 float X( Vector* pObject )
{
    return pObject->x();
}

 float Y( Vector* pObject )
{
    return pObject->y();
}

 float Z( Vector* pObject )
{
    return pObject->z();
}

 MeshPoint *AddPoint( const Vector &v )
{
    // points are held in a sorted map, where the key of
    // a point is its vector distance from (0,0,0) times 10.
    float x = v.x();
    float y = v.y();
    float z = v.z();
    unsigned long key = (unsigned long)(10.0f * (
        (double)x * (double)x +
        (double)y * (double)y +
        (double)z * (double)z));

    // look at points in the vicinity of the point we're adding
    // and see if any of them is close enough to the new point
    PointsMap::iterator it0 = _points.lower_bound(key - 1);
    if (it0 == _points.end())
        it0 = _points.lower_bound(key);
    PointsMap::iterator it1 = _points.upper_bound(key + 1);

    PointsMap::iterator it = it0;
    while (it != it1) {
        const MeshPoint &mpoint = (*it).second;
        const Vector *v2 = &mpoint.point;
        if (fabs(x - v2->x()) < TOLERANCE &&
            fabs(y - v2->y()) < TOLERANCE &&
            fabs(z - v2->z()) < TOLERANCE)
                return (MeshPoint *)&mpoint; // v2
        ++it;
    }

    // if match not found, create a new point
    MeshPoint mpoint;
    mpoint.point = v;
    PointsMap::value_type pair(key, mpoint);
    if (it0 != _points.end())
        it = _points.insert(it0, pair);
    else
        it = _points.insert(pair);
    const MeshPoint &newmpoint = (*it).second;
    //return &newmpoint.point;
    return (MeshPoint *)&newmpoint;
}

 void SetMeshPointNormal( MeshPoint *mp, float x, float y, float z )
{
    mp->normal = Vector( x, y, z );
}

 MeshPlane *AddPlaneToVirtualMesh( MeshFace* face )
{
    Plane p( face->vertex(0), face->vertex(1), face->vertex(2) );
    return addPlane(p);
}

 MeshFace *CreateFacePtr( MeshPoint *p0, MeshPoint *p1, MeshPoint *p2 )
{
    MeshFace *face = new MeshFace(
        &p2->point, &p1->point, &p0->point );

    return face;
}

 void AddFace( MeshFace *face, MeshPlane *mplane )
{
    // compute the area of the face, and drop it if
    // the area is too small
    double face_a = (*face->_v[1] - *face->_v[0]).length();
    double face_b = (*face->_v[2] - *face->_v[1]).length();
    double face_c = (*face->_v[0] - *face->_v[2]).length();
    double face_s = (face_a + face_b + face_c) / 2.0f;
    double face_area2 = face_s * (face_s - face_a) * (face_s - face_b) * (face_s - face_c);
    if (face_area2 < 1e-7)
        return;

    // find the plane for the face
    if (! mplane) {
        Plane p(face->vertex(0), face->vertex(1), face->vertex(2));
        mplane = addPlane(p);
    }

    // make sure the same face hasn't been added before
    const Vector *newVertex0 = face->vertexPtr(0);
    const Vector *newVertex1 = face->vertexPtr(1);
    const Vector *newVertex2 = face->vertexPtr(2);

    // the face hasn't been added before so add it now
    mplane->faces.push_back(face);

    // add a reference to the face from each of its vertices
    MeshPoint *mpoint0 = (MeshPoint *)newVertex0;
    MeshPoint *mpoint1 = (MeshPoint *)newVertex1;
    MeshPoint *mpoint2 = (MeshPoint *)newVertex2;
    mpoint0->faces.push_back(face);
    mpoint1->faces.push_back(face);
    mpoint2->faces.push_back(face);

    //_boundsCached = false;
}

MeshPlane *addPlane( Plane plane1 )
{
    Plane planeToAdd = plane1.normalized();

    PlanesList::iterator itPlanes = _planes.begin();

    if (itPlanes != _planes.end()) {
        const Plane *plane2 = &(*itPlanes).p;
        if (fabs(planeToAdd.a() - plane2->a()) < TOLERANCE &&
            fabs(planeToAdd.b() - plane2->b()) < TOLERANCE &&
            fabs(planeToAdd.c() - plane2->c()) < TOLERANCE &&
            fabs(planeToAdd.d() - plane2->d()) < TOLERANCE)
                return &(*itPlanes);
        while (1) {
            ++itPlanes;
            if (itPlanes == _planes.end())
                break;
            plane2 = &(*itPlanes).p;
            if (fabs(planeToAdd.a() - plane2->a()) < TOLERANCE &&
                fabs(planeToAdd.b() - plane2->b()) < TOLERANCE &&
                fabs(planeToAdd.c() - plane2->c()) < TOLERANCE &&
                fabs(planeToAdd.d() - plane2->d()) < TOLERANCE)
                return &(*itPlanes);
        }
    }

    MeshPlane mpNew;
    mpNew.p = planeToAdd;
    _planes.push_back(mpNew);
    return &(_planes.back());
}
